import ConfigParser
import settings

config = ConfigParser.RawConfigParser()
conf_file = settings.INI_FILE

config.read(conf_file)

is_exist = True

try:
    config.get('general', 'debug')
except (ConfigParser.NoOptionError, ConfigParser.NoSectionError):
    is_exist = False

if not is_exist:
    cfgfile = open(settings.INI_FILE, 'w')
    config.add_section('general')
    config.set('general', 'title', 'pyBSAP')
    config.set('general', 'debug', 'false')
    config.add_section('bsap')
    config.set('bsap', 'baudrate', '9600')
    config.write(cfgfile)
    cfgfile.close()
    print 'New Configuration File is created.'
