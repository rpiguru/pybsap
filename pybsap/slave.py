from pybsap.base import PyBSAPBase


class PyBSAPSlave(PyBSAPBase):

    poll_period = 40
    '''
    The poll period for the Slave node is simply set to a value 1.5 to 3.1 times longer than
    the poll period for the corresponding Master node
    '''

    def __init__(self, **kwargs):
        super(PyBSAPSlave, self).__init__(**kwargs)
